export const Types = {
  GameBusiness: Symbol.for('GameBusiness'),
  GameRepository: Symbol.for('GameRepository'),
  MongoService: Symbol.for('MongoService'),
  MongoProvider: Symbol.for('MongoProvider')
};
