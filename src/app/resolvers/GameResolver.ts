import { Arg, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { container } from '../../inversify/container';
import { Types } from '../../inversify/Types';
import { MyContext } from '../../utils/types/MyContext';
import { IGameBusiness } from '../business/interfaces/IGameBusiness';
import { Game } from '../entities/Game';

@Resolver(Game)
export class GameResolver {
  private _gameBusiness: IGameBusiness;
  constructor() {
    this._gameBusiness = container.get(Types.GameBusiness);
  }

  @Query(() => [Game])
  getAllGames(): Promise<Game[]> {
    return this._gameBusiness.findAll();
  }

  @Mutation(() => Game, { nullable: true })
  async getGameById(@Arg('id') id: string): Promise<Game | undefined> {
    return this._gameBusiness.findById(id);
  }

  @Mutation(() => Game)
  async startNewGame(
    @Ctx() { req }: MyContext,
    @Arg('bet') bet: number
  ): Promise<Game> {
    if (!req.session.player)
      throw new Error('There is no player! Please start session...');
    if (req.session.player.game)
      throw new Error("You can't start several games at the same time!");

    const game = await this._gameBusiness.save(new Game(bet));

    req.session.player.balance -= bet;
    req.session.player.game = game;

    return game;
  }

  @Query(() => Game, { nullable: true })
  getCurrentGame(@Ctx() { req }: MyContext): Game | undefined {
    return req.session.player ? req.session.player.game : undefined;
  }

  @Query(() => Number)
  async applyGameResult(@Ctx() { req }: MyContext): Promise<number> {
    if (!req.session.player)
      throw new Error('There is no game session! Please start this one...');

    const currentGame = req.session.player.game;
    if (!currentGame) throw new Error('No one game started!');

    const gameId = currentGame.id;
    const game = await this._gameBusiness.findById(gameId.toString());
    if (!game) throw new Error(); // вряд ли такая ситуация возможна в данном кейсе, но пусть будет

    const { result } = game;
    if (result && result >= 0) {
      req.session.player.balance += result;
    }

    req.session.player.game = undefined;

    return req.session.player.balance;
  }
}
