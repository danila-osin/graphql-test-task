import { Ctx, Query, Resolver } from 'type-graphql';
import { Player } from '../../utils/classes/Player';
import { MyContext } from '../../utils/types/MyContext';
@Resolver(Player)
export class PlayerResolver {
  @Query(() => Player, { nullable: true })
  startSession(@Ctx() { req }: MyContext): Player {
    const newPlayer = new Player();
    req.session.player = newPlayer;
    return newPlayer;
  }

  @Query(() => Player, { nullable: true })
  getSession(@Ctx() { req }: MyContext): Player | undefined {
    return req.session.player;
  }

  @Query(() => Number, { nullable: true })
  getBalance(@Ctx() { req }: MyContext): number | undefined {
    return req.session.player ? req.session.player.balance : undefined;
  }
}
