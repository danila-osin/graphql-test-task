import { Game } from '../../entities/Game';

export interface IGameRepository {
  save(game: Game): Promise<Game>;
  findById(id: string): Promise<Game | undefined>;
  findAll(): Promise<Game[]>;
}
