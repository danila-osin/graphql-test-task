import { injectable, unmanaged } from 'inversify';
import { Connection, MongoRepository } from 'typeorm';
import { container } from '../../../inversify/container';
import { Types } from '../../../inversify/Types';
import { IMongoProvider } from '../../services/interfaces/IMongoProvider';

@injectable()
export class BaseRepository<EntityType> {
  private _mongoProvider: IMongoProvider;

  constructor(@unmanaged() private _EntityClass: new (...args: any[]) => any) {
    this._mongoProvider = container.get(Types.MongoProvider);
  }

  private getConnection(): Connection {
    return this._mongoProvider();
  }

  get repository(): MongoRepository<EntityType> {
    return this.getConnection().getMongoRepository(this._EntityClass);
  }
}
