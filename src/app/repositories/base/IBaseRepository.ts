import { Connection, MongoRepository } from 'typeorm';

export interface IBaseRepository<EntityType> {
  getConnection(): Connection;
  repository: MongoRepository<EntityType>;
}
