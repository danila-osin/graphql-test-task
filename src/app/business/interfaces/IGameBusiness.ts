import { Game } from '../../entities/Game';

export interface IGameBusiness {
  save(game: Game): Promise<Game>;
  findById(id: string): Promise<Game | undefined>;
  findAll(): Promise<Game[]>;
}
