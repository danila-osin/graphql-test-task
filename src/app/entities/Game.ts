import { Field, ObjectType } from 'type-graphql';
import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { sleep } from '../../utils/handlers/sleep';

@ObjectType()
@Entity()
export class Game {
  @Field(() => String)
  @ObjectIdColumn()
  id!: ObjectID;

  @Field()
  @Column({ type: 'int' })
  bet!: number;

  @Field({ nullable: true })
  @Column({ type: 'int', nullable: true })
  result?: number;

  @Field()
  @Column({ type: 'int' })
  chanceToWin!: number;

  constructor(bet: number, chanceToWin: number = 49) {
    this.bet = bet;
    this.chanceToWin = chanceToWin;
  }

  async calculateResult() {
    await sleep(5);

    const randomValue = Math.floor(Math.random() * 100 + 1); // betweet 1 and 100 (1, 100 included)

    if (randomValue <= this.chanceToWin) {
      this.result = this.bet * 2;
    } else {
      this.result = this.bet * -1;
    }
  }
}
