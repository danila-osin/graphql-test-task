import { Connection } from 'typeorm';

export interface IMongoProvider {
  (): Connection;
}
