import { Field, ObjectType } from 'type-graphql';
import { Game } from '../../app/entities/Game';

@ObjectType()
export class Player {
  @Field()
  balance: number;

  @Field(() => Game, { nullable: true })
  game?: Game;

  constructor(balance: number = 100) {
    this.balance = balance;
  }
}
