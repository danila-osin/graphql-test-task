import { Session } from 'express-session';
import { Player } from '../classes/Player';

export type GameSession = Session & { player?: Player };
