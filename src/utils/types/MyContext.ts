import { Request, Response } from 'express';
import { Redis } from 'ioredis';
import { GameSession } from './GameSession';

export type MyContext = {
  req: Request & { session: GameSession };
  res: Response;
  redis: Redis;
};
